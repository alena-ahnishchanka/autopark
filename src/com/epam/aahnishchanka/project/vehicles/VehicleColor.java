package com.epam.aahnishchanka.project.vehicles;

/**
 * The {@code VehicleColor} enum represents possible values of vehicle color
 *
 * @author Alena Ahnishchanka
 */

public enum VehicleColor {
    WHITE, BLACK, GREY, BROWN, RED, PINK, ORANGE, YELLOW, GREEN, BLUE, PURPLE, MIXED
}
