package com.epam.aahnishchanka.project.vehicles;

/**
 * The {@code EcoStandard} enum represents possible values of vehicle eco standard
 *
 * @author Alena Ahnishchanka
 */

public enum EcoStandard {
    EURO1, EURO2, EURO3, EURO4, EURO5, EURO6, OTHER, NONE
}
