package com.epam.aahnishchanka.project.vehicles.nonMotorizedVehicles;

import com.epam.aahnishchanka.project.vehicles.VehicleColor;
import com.epam.aahnishchanka.project.vehicles.WaterVehicle;
import com.epam.aahnishchanka.project.vehicles.NonMotorizedVehicle;

public class Catamaran extends NonMotorizedVehicle implements WaterVehicle {

    private int lifeJacketsNumber;

    /**
     * @param vehicleNumber     The number of catamaran
     * @param color             The color of catamaran
     * @param brand             The brand of catamaran
     * @param capacity          The capacity of catamaran
     * @param eligibleForAbroad The eligibility for abroad (true in case catamaran is eligible for abroad)
     * @param lifeJacketsNumber The number of life jackets on catamaran
     */
    public Catamaran(String vehicleNumber, VehicleColor color, String brand, int capacity, boolean eligibleForAbroad,
                     int lifeJacketsNumber) {
        this.vehicleNumber = vehicleNumber;
        this.color = color;
        this.brand = brand;
        this.capacity = capacity;
        this.eligibleForAbroad = eligibleForAbroad;
        this.lifeJacketsNumber = lifeJacketsNumber;
    }

    /**
     * @return number of life jackets on catamaran
     */
    @Override
    public int getLifeJacketsNumber() {
        return lifeJacketsNumber;
    }

    /**
     * @param lifeJacketsNumber sets number of life jackets on catamaran
     */
    @Override
    public void setLifeJacketsNumber(int lifeJacketsNumber) {
        this.lifeJacketsNumber = lifeJacketsNumber;
    }

    /**
     * @return the list of catamaran field values in human readable format
     */
    @Override
    public String toString() {

        String fieldValues;
        fieldValues = "CATAMARAN ("
                .concat("number: ").concat(vehicleNumber)
                .concat("; color: ").concat(String.valueOf(color).toLowerCase())
                .concat("; brand: ").concat(String.valueOf(brand))
                .concat("; capacity: ").concat(String.valueOf(capacity))
                .concat("; eligible for abroad: ").concat(String.valueOf(eligibleForAbroad))
                .concat("; life jackets number: ").concat(String.valueOf(lifeJacketsNumber))
                .concat(")");

        return fieldValues;
    }
}
