package com.epam.aahnishchanka.project.vehicles.nonMotorizedVehicles;

import com.epam.aahnishchanka.project.vehicles.VehicleColor;
import com.epam.aahnishchanka.project.vehicles.NonMotorizedVehicle;

public class Bike extends NonMotorizedVehicle {

    private boolean hasFlashlight;

    /**
     * @param vehicleNumber     The number of bike
     * @param color             The color of bike
     * @param brand             The brand of bike
     * @param capacity          The capacity of bike
     * @param eligibleForAbroad The eligibility for abroad (true in case bike is eligible for abroad)
     * @param hasFlashlight     The presence of flashlight (true in case bike has flashlight)
     */
    public Bike(String vehicleNumber, VehicleColor color, String brand, int capacity, boolean eligibleForAbroad,
                boolean hasFlashlight) {
        this.vehicleNumber = vehicleNumber;
        this.color = color;
        this.brand = brand;
        this.capacity = capacity;
        this.eligibleForAbroad = eligibleForAbroad;
        this.hasFlashlight = hasFlashlight;
    }

    /**
     * @return presence of flashlight on bike (true in case bike has flashlight, false if not)
     */
    public boolean getHasFlashlight() {
        return hasFlashlight;
    }

    /**
     * @param hasFlashlight sets presence of flashlight on bike (true in case bike has flashlight, false if not)
     */
    public void setHasFlashlight(boolean hasFlashlight) {
        this.hasFlashlight = hasFlashlight;
    }

    /**
     * @return the list of bike field values in human readable format
     */
    @Override
    public String toString() {

        String fieldValues;
        fieldValues = "BIKE ("
                .concat("number: ").concat(vehicleNumber)
                .concat("; color: ").concat(String.valueOf(color).toLowerCase())
                .concat("; brand: ").concat(String.valueOf(brand))
                .concat("; capacity: ").concat(String.valueOf(capacity))
                .concat("; eligible for abroad: ").concat(String.valueOf(eligibleForAbroad))
                .concat("; has flashlight: ").concat(String.valueOf(hasFlashlight))
                .concat(")");

        return fieldValues;
    }
}
