package com.epam.aahnishchanka.project.vehicles;

/**
 * The {@code MotorizedVehicle} class represents motorized vehicle and its required parameters and methods
 *
 * @author Alena Ahnishchanka
 */

public abstract class MotorizedVehicle extends Vehicle {

    protected float gasConsumption;
    protected float driveRange;
    protected boolean hasAirConditioner;
    protected EcoStandard ecoStandard;

    /**
     * @return motorized vehicle gas consumption
     */
    public float getGasConsumption() {
        return gasConsumption;
    }

    /**
     * @return motorized vehicle drive range
     */
    public float getDriveRange() {
        return driveRange;
    }

    /**
     * @return presence of air conditioner on motorized vehicle (true if air conditioner exists, false if not)
     */
    public boolean getHasAirConditioner() {
        return hasAirConditioner;
    }

    /**
     * @return motorized vehicle eco standard
     */
    public EcoStandard getEcoStandard() {
        return ecoStandard;
    }

    /**
     * @param gasConsumption sets motorized vehicle gas consumption
     */
    public void setGasConsumption(float gasConsumption) {
        this.gasConsumption = gasConsumption;
    }

    /**
     * @param driveRange sets motorized vehicle drive range
     */
    public void setDriveRange(float driveRange) {
        this.driveRange = driveRange;
    }

    /**
     * @param hasAirConditioner sets presence of air conditioner on motorized vehicle
     *                          (true if air conditioner exists, false if not)
     */
    public void setHasAirConditioner(boolean hasAirConditioner) {
        this.hasAirConditioner = hasAirConditioner;
    }

    /**
     * @param ecoStandard sets motorized vehicle eco standard
     */
    public void setEcoStandard(EcoStandard ecoStandard) {
        this.ecoStandard = ecoStandard;
    }

}
