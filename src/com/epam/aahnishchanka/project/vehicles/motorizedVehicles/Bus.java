package com.epam.aahnishchanka.project.vehicles.motorizedVehicles;

import com.epam.aahnishchanka.project.vehicles.EcoStandard;
import com.epam.aahnishchanka.project.vehicles.VehicleColor;
import com.epam.aahnishchanka.project.vehicles.MotorizedVehicle;

public class Bus extends MotorizedVehicle {

    private boolean allowedChildren;

    /**
     * @param vehicleNumber     The number of bus
     * @param color             The color of bus
     * @param brand             The brand of bus
     * @param capacity          The capacity of bus
     * @param eligibleForAbroad The eligibility for abroad (true in case bus is eligible for abroad)
     * @param gasConsumption    The gas consumption of bus
     * @param driveRange        The drive range of bus
     * @param hasAirConditioner The presence of air conditioner (true in case bus has air conditioner)
     * @param ecoStandard       The eco standard of bus
     * @param allowedChildren   The permission to transport children (true in case bus is allowed to transport children)
     */
    public Bus(String vehicleNumber, VehicleColor color, String brand, int capacity, boolean eligibleForAbroad,
               float gasConsumption, float driveRange, boolean hasAirConditioner, EcoStandard ecoStandard,
               boolean allowedChildren) {
        this.vehicleNumber = vehicleNumber;
        this.color = color;
        this.brand = brand;
        this.capacity = capacity;
        this.eligibleForAbroad = eligibleForAbroad;
        this.gasConsumption = gasConsumption;
        this.driveRange = driveRange;
        this.hasAirConditioner = hasAirConditioner;
        this.ecoStandard = ecoStandard;
        this.allowedChildren = allowedChildren;

    }

    /**
     * @return permission to transport children (true in case bus is allowed to transport children, false if not)
     */
    public boolean getAllowedChildren() {
        return allowedChildren;
    }

    /**
     * @param allowedChildren sets permission to transport children
     *                       (true in case bus is allowed to transport children, false if not)
     */
    public void setAllowedChildren(boolean allowedChildren) {
        this.allowedChildren = allowedChildren;
    }

    /**
     * @return the list of bus field values in human readable format
     */
    @Override
    public String toString() {

        String fieldValues;
        fieldValues = "BUS ("
                .concat("number: ").concat(vehicleNumber)
                .concat("; color: ").concat(String.valueOf(color).toLowerCase())
                .concat("; brand: ").concat(String.valueOf(brand))
                .concat("; capacity: ").concat(String.valueOf(capacity))
                .concat("; eligible for abroad: ").concat(String.valueOf(eligibleForAbroad))
                .concat("; gas consumption: ").concat(String.valueOf(gasConsumption))
                .concat("; drive range: ").concat(String.valueOf(driveRange))
                .concat("; equipped with air conditioner: ").concat(String.valueOf(hasAirConditioner))
                .concat("; eco standard: ").concat(String.valueOf(ecoStandard))
                .concat("; allowed for children: ").concat(String.valueOf(allowedChildren))
                .concat(")");

        return fieldValues;
    }
}
