package com.epam.aahnishchanka.project.vehicles.motorizedVehicles;

import com.epam.aahnishchanka.project.vehicles.EcoStandard;
import com.epam.aahnishchanka.project.vehicles.VehicleColor;
import com.epam.aahnishchanka.project.vehicles.MotorizedVehicle;

public class Car extends MotorizedVehicle {

    /**
     * @param vehicleNumber     The number of car
     * @param color             The color of car
     * @param brand             The brand of car
     * @param capacity          The capacity of car
     * @param eligibleForAbroad The eligibility for abroad (true in case car is eligible for abroad)
     * @param gasConsumption    The gas consumption of car
     * @param driveRange        The drive range of car
     * @param hasAirConditioner The presence of air conditioner (true in case car has air conditioner)
     * @param ecoStandard       The eco standard of car
     */
    public Car(String vehicleNumber, VehicleColor color, String brand, int capacity, boolean eligibleForAbroad,
               float gasConsumption, float driveRange, boolean hasAirConditioner, EcoStandard ecoStandard) {
        this.vehicleNumber = vehicleNumber;
        this.color = color;
        this.brand = brand;
        this.capacity = capacity;
        this.eligibleForAbroad = eligibleForAbroad;
        this.gasConsumption = gasConsumption;
        this.driveRange = driveRange;
        this.hasAirConditioner = hasAirConditioner;
        this.ecoStandard = ecoStandard;
    }

    /**
     * @return the list of car field values in human readable format
     */
    @Override
    public String toString() {

        String fieldValues;
        fieldValues = "CAR ("
                .concat("number: ").concat(vehicleNumber)
                .concat("; color: ").concat(String.valueOf(color).toLowerCase())
                .concat("; brand: ").concat(String.valueOf(brand))
                .concat("; capacity: ").concat(String.valueOf(capacity))
                .concat("; eligible for abroad: ").concat(String.valueOf(eligibleForAbroad))
                .concat("; gas consumption: ").concat(String.valueOf(gasConsumption))
                .concat("; drive range: ").concat(String.valueOf(driveRange))
                .concat("; equipped with air conditioner: ").concat(String.valueOf(hasAirConditioner))
                .concat("; eco standard: ").concat(String.valueOf(ecoStandard))
                .concat(")");

        return fieldValues;
    }
}
