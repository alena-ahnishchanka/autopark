package com.epam.aahnishchanka.project.vehicles.motorizedVehicles;

import com.epam.aahnishchanka.project.vehicles.EcoStandard;
import com.epam.aahnishchanka.project.vehicles.VehicleColor;
import com.epam.aahnishchanka.project.vehicles.WaterVehicle;
import com.epam.aahnishchanka.project.vehicles.MotorizedVehicle;

public class Boat extends MotorizedVehicle implements WaterVehicle {

    private int lifeJacketsNumber;

    /**
     * @param vehicleNumber     The number of boat
     * @param color             The color of boat
     * @param brand             The brand of boat
     * @param capacity          The capacity of boat
     * @param eligibleForAbroad The eligibility for abroad (true in case boat is eligible for abroad)
     * @param gasConsumption    The gas consumption of boat
     * @param driveRange        The drive range of boat
     * @param hasAirConditioner The presence of air conditioner (true in case boat has air conditioner)
     * @param ecoStandard       The eco standard of boat
     * @param lifeJacketsNumber The number of life jackets on boat
     */
    public Boat(String vehicleNumber, VehicleColor color, String brand, int capacity, boolean eligibleForAbroad,
                float gasConsumption, float driveRange, boolean hasAirConditioner, EcoStandard ecoStandard,
                int lifeJacketsNumber) {
        this.vehicleNumber = vehicleNumber;
        this.color = color;
        this.brand = brand;
        this.capacity = capacity;
        this.eligibleForAbroad = eligibleForAbroad;
        this.gasConsumption = gasConsumption;
        this.driveRange = driveRange;
        this.hasAirConditioner = hasAirConditioner;
        this.ecoStandard = ecoStandard;
        this.lifeJacketsNumber = lifeJacketsNumber;
    }

    /**
     * @return number of life jackets on boat
     */
    @Override
    public int getLifeJacketsNumber() {
        return lifeJacketsNumber;
    }

    /**
     * @param lifeJacketsNumber sets number of life jackets on boat
     */
    @Override
    public void setLifeJacketsNumber(int lifeJacketsNumber) {
        this.lifeJacketsNumber = lifeJacketsNumber;
    }

    /**
     * @return the list of boat field values in human readable format
     */
    @Override
    public String toString() {

        String fieldValues;
        fieldValues = "BOAT ("
                .concat("number: ").concat(vehicleNumber)
                .concat("; color: ").concat(String.valueOf(color).toLowerCase())
                .concat("; brand: ").concat(brand)
                .concat("; capacity: ").concat(String.valueOf(capacity))
                .concat("; eligible for abroad: ").concat(String.valueOf(eligibleForAbroad))
                .concat("; gas consumption: ").concat(String.valueOf(gasConsumption))
                .concat("; drive range: ").concat(String.valueOf(driveRange))
                .concat("; equipped with air conditioner: ").concat(String.valueOf(hasAirConditioner))
                .concat("; eco standard: ").concat(String.valueOf(ecoStandard))
                .concat("; life jackets number: ").concat(String.valueOf(lifeJacketsNumber))
                .concat(")");

        return fieldValues;
    }
}
