package com.epam.aahnishchanka.project.vehicles;

/**
 * The {@code NonMotorizedVehicle} class represents non-motorized vehicle and its required parameters
 * and methods
 *
 * @author Alena Ahnishchanka
 */

public abstract class NonMotorizedVehicle extends Vehicle {

}
