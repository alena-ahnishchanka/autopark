package com.epam.aahnishchanka.project.vehicles;

/**
 * The {@code WaterVehicle} interface represents specification of water vehicle (its required methods)
 *
 * @author Alena Ahnishchanka
 */

public interface WaterVehicle {

    /**
     * @return number of life jackets on water vehicle
     */
    int getLifeJacketsNumber();

    /**
     * @param lifeJacketsNumber sets number of life jackets on water vehicle
     */
    void setLifeJacketsNumber(int lifeJacketsNumber);
}
