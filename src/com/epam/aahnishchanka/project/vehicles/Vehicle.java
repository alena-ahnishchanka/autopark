package com.epam.aahnishchanka.project.vehicles;

/**
 * The {@code Vehicle} base class represents vehicle of any type and its required parameters and methods
 *
 * @author Alena Ahnishchanka
 */

public abstract class Vehicle {

    protected String vehicleNumber;
    protected VehicleColor color;
    protected String brand;
    protected int capacity;
    protected boolean eligibleForAbroad;

    /**
     * @return vehicle number
     */
    public String getVehicleNumber() {
        return vehicleNumber;
    }

    /**
     * @return vehicle color
     */
    public VehicleColor getColor() {
        return color;
    }

    /**
     * @return vehicle brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @return vehicle capacity
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * @return true if vehicle is eligible for abroad, false if not
     */
    public boolean getEligibleForAbroad() {
        return eligibleForAbroad;
    }

    /**
     * @param vehicleNumber sets vehicle number
     */
    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    /**
     * @param color sets vehicle color
     */
    public void setColor(VehicleColor color) {
        this.color = color;
    }

    /**
     * @param brand sets vehicle brand
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * @param capacity sets vehicle capacity
     */
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    /**
     * @param eligibleForAbroad sets vehicle eligibility for abroad
     *                          (true if vehicle is eligible for abroad, false if not)
     */
    public void setEligibleForAbroad(boolean eligibleForAbroad) {
        this.eligibleForAbroad = eligibleForAbroad;
    }
}