package com.epam.aahnishchanka.project;

import com.epam.aahnishchanka.project.autopark.Autopark;

import java.util.Arrays;

import static com.epam.aahnishchanka.project.vehicles.EcoStandard.*;
import static com.epam.aahnishchanka.project.vehicles.VehicleColor.*;

/**
 * The {@code Main} class is a main class for program running.
 *
 * @author Alena Ahnishchanka
 */

public class Main {

    public static void main(String[] args) {

        Autopark autopark1 = new Autopark();

        autopark1.addCar("1", BLUE, "Mersedes", 4, true, 6,
                900, true, EURO5);
        autopark1.addCar("2", BLUE, "Audi", 4, true, 6,
                900, true, EURO6);
        autopark1.addCar("3", BLUE, "Volkswagen", 4, false, 6,
                1900, true, EURO5);
        autopark1.addBoat("4", BLUE, "Bass Cat", 10, true,
                8.5f, 1500, true, EURO4, 10);
        autopark1.addBoat("5", BLUE, "Bass Cat", 10, true,
                8.5f, 1500, true, EURO4, 10);
        autopark1.addBike("6", BLACK, "Specialized", 2, false,
                true);
        autopark1.addCar("7", BLUE, "Chevrolet", 2, true, 6,
                900, true, EURO6);
        autopark1.addCar("8", BLUE, "Bentley", 4, true, 6,
                900, true, EURO6);
        autopark1.addCar("9", BLUE, "Cadillac", 5, true, 6,
                900, true, EURO3);
        autopark1.addBus("10", GREY, "Ikarus", 65, false, 6,
                900, false, OTHER, true);
        autopark1.addBus("11", GREY, "Ikarus", 65, false, 6,
                900, false, OTHER, true);
        autopark1.addBus("12", GREEN, "Ikarus", 65, false, 6,
                900, false, EURO2, true);
        autopark1.addBike("13", GREEN, "GT", 1, true, false);
        autopark1.addBoat("14", BLUE, "BoatMate", 50, false, 6,
                900, true, OTHER, 50);
        autopark1.addBike("15", WHITE, "Trek", 1, true, true);
        autopark1.addCar("16", BLUE, "BMW", 4, true, 6,
                900, true, EURO5);
        autopark1.addBus("17", WHITE, "Mersedes", 65, false, 6,
                900, true, EURO6, true);
        autopark1.addCar("18", BLUE, "Chevrolet", 2, true, 6,
                900, true, EURO6);
        autopark1.addCar("19", BLACK, "Lexus", 4, true, 6,
                900, true, EURO5);
        autopark1.addBoat("20", WHITE, "Bass Cat", 4, false, 6,
                900, true, OTHER, 4);
        autopark1.addBike("21", YELLOW, "Bianchi", 1, true, true);
        autopark1.addBus("22", RED, "Fiat", 65, false, 6,
                900, true, EURO6, true);
        autopark1.addBus("23", WHITE, "Fiat", 65, false, 6,
                900, true, EURO5, false);
        autopark1.addCatamaran("24", YELLOW, "AQUILA", 4, true,
                4);
        autopark1.addTrack("25", BLACK, "Tiger-I", 4, true, 6,
                900, true, NONE);

        autopark1.printAllVehiclesToConsole();

        autopark1.delete("1");

        autopark1.delete("27");

        autopark1.printAllVehiclesToConsole();

        autopark1.deleteList(Arrays.asList("1", "3", "4", "15"));

        autopark1.printAllVehiclesToConsole();

        autopark1.deleteList(Arrays.asList("55", "20", "21", "21"));

        autopark1.printAllVehiclesToConsole();
    }
}
