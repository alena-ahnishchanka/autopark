package com.epam.aahnishchanka.project.autopark;

import com.epam.aahnishchanka.project.vehicles.EcoStandard;
import com.epam.aahnishchanka.project.vehicles.Vehicle;
import com.epam.aahnishchanka.project.vehicles.VehicleColor;
import com.epam.aahnishchanka.project.vehicles.motorizedVehicles.*;
import com.epam.aahnishchanka.project.vehicles.nonMotorizedVehicles.*;

import java.util.ArrayList;
import java.util.List;

/**
 * The {@code Autopark} class represents creation of object with vehicles list (autopark)
 * and actions that can be performed on it.
 *
 * @author Alena Ahnishchanka
 */

public class Autopark {

    private List<Vehicle> vehiclesList = new ArrayList<>();

    /**
     * Adds a new bus to the autopark
     *
     * @param vehicleNumber     The number of bus
     * @param color             The color of bus
     * @param brand             The brand of bus
     * @param capacity          The capacity of bus
     * @param eligibleForAbroad The eligibility for abroad (true in case bus is eligible for abroad)
     * @param gasConsumption    The gas consumption of bus
     * @param driveRange        The drive range of bus
     * @param hasAirConditioner The presence of air conditioner (true in case bus has air conditioner)
     * @param ecoStandard       The eco standard of bus
     * @param allowedChildren   The permission to transport children (true in case bus is allowed to transport children)
     */
    public void addBus(String vehicleNumber, VehicleColor color, String brand, int capacity, boolean eligibleForAbroad,
                       float gasConsumption, float driveRange, boolean hasAirConditioner, EcoStandard ecoStandard,
                       boolean allowedChildren) {
        vehiclesList.add(new Bus(vehicleNumber, color, brand, capacity, eligibleForAbroad, gasConsumption, driveRange,
                hasAirConditioner, ecoStandard, allowedChildren));
    }

    /**
     * Adds a new car to the autopark
     *
     * @param vehicleNumber     The number of car
     * @param color             The color of car
     * @param brand             The brand of car
     * @param capacity          The capacity of car
     * @param eligibleForAbroad The eligibility for abroad (true in case car is eligible for abroad)
     * @param gasConsumption    The gas consumption of car
     * @param driveRange        The drive range of car
     * @param hasAirConditioner The presence of air conditioner (true in case car has air conditioner)
     * @param ecoStandard       The eco standard of car
     */
    public void addCar(String vehicleNumber, VehicleColor color, String brand, int capacity, boolean eligibleForAbroad,
                       float gasConsumption, float driveRange, boolean hasAirConditioner, EcoStandard ecoStandard) {
        vehiclesList.add(new Car(vehicleNumber, color, brand, capacity, eligibleForAbroad, gasConsumption, driveRange,
                hasAirConditioner, ecoStandard));
    }

    /**
     * Adds a new track to the autopark
     *
     * @param vehicleNumber     The number of track
     * @param color             The color of track
     * @param brand             The brand of track
     * @param capacity          The capacity of track
     * @param eligibleForAbroad The eligibility for abroad (true in case track is eligible for abroad)
     * @param gasConsumption    The gas consumption of track
     * @param driveRange        The drive range of track
     * @param hasAirConditioner The presence of air conditioner (true in case track has air conditioner)
     * @param ecoStandard       The eco standard of track
     */
    public void addTrack(String vehicleNumber, VehicleColor color, String brand, int capacity,
                         boolean eligibleForAbroad, float gasConsumption, float driveRange, boolean hasAirConditioner,
                         EcoStandard ecoStandard) {
        vehiclesList.add(new Track(vehicleNumber, color, brand, capacity, eligibleForAbroad, gasConsumption, driveRange,
                hasAirConditioner, ecoStandard));
    }

    /**
     * Adds a new bike to the autopark
     *
     * @param vehicleNumber     The number of bike
     * @param color             The color of bike
     * @param brand             The brand of bike
     * @param capacity          The capacity of bike
     * @param eligibleForAbroad The eligibility for abroad (true in case bike is eligible for abroad)
     * @param hasFlashlight     The presence of flashlight (true in case bike has flashlight)
     */
    public void addBike(String vehicleNumber, VehicleColor color, String brand, int capacity, boolean eligibleForAbroad,
                        boolean hasFlashlight) {
        vehiclesList.add(new Bike(vehicleNumber, color, brand, capacity, eligibleForAbroad, hasFlashlight));
    }

    /**
     * Adds a new boat to the autopark
     *
     * @param vehicleNumber     The number of boat
     * @param color             The color of boat
     * @param brand             The brand of boat
     * @param capacity          The capacity of boat
     * @param eligibleForAbroad The eligibility for abroad (true in case boat is eligible for abroad)
     * @param gasConsumption    The gas consumption of boat
     * @param driveRange        The drive range of boat
     * @param hasAirConditioner The presence of air conditioner (true in case boat has air conditioner)
     * @param ecoStandard       The eco standard of boat
     * @param lifeJacketsNumber The number of life jackets on boat
     */
    public void addBoat(String vehicleNumber, VehicleColor color, String brand, int capacity, boolean eligibleForAbroad,
                        float gasConsumption, float driveRange, boolean hasAirConditioner, EcoStandard ecoStandard,
                        int lifeJacketsNumber) {
        vehiclesList.add(new Boat(vehicleNumber, color, brand, capacity, eligibleForAbroad, gasConsumption, driveRange,
                hasAirConditioner, ecoStandard, lifeJacketsNumber));
    }

    /**
     * Adds a new catamaran to the autopark
     *
     * @param vehicleNumber     The number of catamaran
     * @param color             The color of catamaran
     * @param brand             The brand of catamaran
     * @param capacity          The capacity of catamaran
     * @param eligibleForAbroad The eligibility for abroad (true in case catamaran is eligible for abroad)
     * @param lifeJacketsNumber The number of life jackets on catamaran
     */
    public void addCatamaran(String vehicleNumber, VehicleColor color, String brand, int capacity,
                             boolean eligibleForAbroad, int lifeJacketsNumber) {
        vehiclesList.add(new Catamaran(vehicleNumber, color, brand, capacity, eligibleForAbroad, lifeJacketsNumber));
    }

    /**
     * Deletes the vehicle from the autopark by its number
     *
     * @return true if vehicle was deleted, false - if vehicle with requested number was not found
     */
    public boolean delete(String vehicleNumber) {
        return vehiclesList.removeIf((v) -> vehicleNumber.equals(v.getVehicleNumber()));
    }

    /**
     * Deletes several vehicles from the autopark by their numbers
     *
     * @return true if vehicle was deleted, false - if vehicle with requested number was not found
     */
    public boolean deleteList(List<String> vehicleNumbers) {
        return vehiclesList.removeIf((v) -> vehicleNumbers.stream().anyMatch(v2->v2.equals(v.getVehicleNumber())));
    }

    /**
     * Prints the list of vehicles in the autopark with their parameters values to console
     */
    public void printAllVehiclesToConsole() {
        System.out.println("\nLIST OF AUTOPARK VEHICLES:\n--------------------------");
        vehiclesList.forEach(System.out::println);
    }

}
